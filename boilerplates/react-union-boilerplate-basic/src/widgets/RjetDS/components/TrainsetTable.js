/* eslint-disable react/prefer-stateless-function */
import PropTypes from 'prop-types';
import Trainset from './Trainset';

const React = require('react');

class TrainsetTable extends React.Component {
	render() {
		const rows = [];
		Array.prototype.forEach.call(this.props.trainsets, trainset => {
			rows.push(<Trainset key={trainset.id} trainset={trainset} showRoutes={this.props.showRoutes} />);
		});

		return (
			<div className="container">
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Soupravy</th>
							<th>Vozy</th>
							<th>Vlaky</th>
							<th>Akce</th>
						</tr>
					</thead>
					<tbody>{rows}</tbody>
				</table>
			</div>
		);
	}
}

TrainsetTable.propTypes = {
	showRoutes: PropTypes.func,
	trainsets: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
};

export default TrainsetTable;
