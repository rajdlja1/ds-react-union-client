import renderRoute from './Route';

const React = require('react');

function renderRouteTable(routes) {
	const rows = [];
	Array.prototype.forEach.call(routes, route => {
		rows.push(renderRoute(route));
	});

	return (
		<div className="container">
			<table className="table table-striped">
				<thead>
					<tr>
						<th>MZ</th>
						<th>Souprava</th>
						<th>GVD odj.</th>
						<th>Z žst.</th>
						<th>GVS příj.</th>
						<th>Do žst.</th>
						<th>Strojvedoucí</th>
						<th>Telefon</th>
						<th>Vozy</th>
						<th>Akce</th>
					</tr>
				</thead>
				<tbody>{rows}</tbody>
			</table>
		</div>
	);
}

export default renderRouteTable;
