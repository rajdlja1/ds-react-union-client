const React = require('react');

function renderCar(car, setId) {
	return <span key={`${setId}-${car.id}-${car.description}`}>{car.name}</span>;
}

function renderRoute(route) {
	const routeId = route.id;
	const cars = [];
	route.trainSet.cars.forEach(function(car) {
		cars.push(renderCar(car, routeId));
	});

	return (
		<tr key={route.id}>
			<td>{route.extraStop}</td>
			<td>{route.trainSet.name}</td>
			<td>{route.departureTime}</td>
			<td>{route.from}</td>
			<td>{route.arrivalTime}</td>
			<td>{route.to}</td>
			<td>{route.driver.firstname} {route.driver.lastname}</td>
			<td>{route.driver.phoneNumber}</td>
			<td>{cars.map(c => c).reduce((prev, curr) => [prev, ', ', curr])}</td>
			<td>
				<button className="btn btn-info">Editovat spoj</button>
			</td>
		</tr>);
}

export default renderRoute;
