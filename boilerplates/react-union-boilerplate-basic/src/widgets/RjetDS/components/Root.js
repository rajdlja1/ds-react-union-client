import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SearchForm from './SearchForm';
import TrainsetService from './TrainsetService';
import TrainsetTable from './TrainsetTable';
import renderRouteTable from './RouteTable';

const React = require('react');

class App extends React.Component {
	static DS_TABLE_TITLE = 'Dispečerská tabule';
	static TRAINSETS_TITLE = 'Soupravy';
	static ROUTES_TITLE = 'Spoje';

	static TRAINSETS_SCREEN = 'TRAINSET_SCREEN';

	constructor(props) {
		super(props);
		this.state = { trainsets: [], routes: [], mode: App.TRAINSETS_SCREEN };
		this.reloadTrainsetsFromServer = TrainsetService.reloadTrainsetsFromServer.bind(this);
		this.loadTrainsetsFromServer = TrainsetService.loadTrainsetsFromServer.bind(this);
		this.showRoutesOfTrainset = TrainsetService.showRoutes.bind(this);
	}

	componentDidMount() {
		this.loadTrainsetsFromServer();
	}

	render() {
		if (this.state.mode === App.TRAINSETS_SCREEN)
			return (
				<div className="container">
					<h2>{App.DS_TABLE_TITLE}</h2>
					<h3>{App.TRAINSETS_TITLE}</h3>
					<MuiThemeProvider>
						<SearchForm handleFilterTrainsets={this.reloadTrainsetsFromServer} />
					</MuiThemeProvider>
					<TrainsetTable
						trainsets={this.state.trainsets}
						showRoutes={this.showRoutesOfTrainset}
					/>
				</div>
			);

		return (
			<div className="container">
				<h2>{App.DS_TABLE_TITLE}</h2>
				<h3>{App.ROUTES_TITLE}</h3>
				{renderRouteTable(this.state.routes)}
			</div>
		);
	}
}

const Root = () => {
	return <App />;
};

export default Root;
