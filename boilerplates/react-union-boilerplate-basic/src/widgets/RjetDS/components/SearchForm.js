import PropTypes from 'prop-types';
import Datepicker from './Datepicker';
import getFormattedDate from './DateFormatter';

const React = require('react');

class SearchForm extends React.Component {
	static ON_THE_WAY = 'NACESTE';
	static ARRIVAL = 'PRIJEZD';
	static DEPARTURE = 'ODJEZD';

	static SEARCH_ON_THE_WAY_LABEL = 'Jedou teď';
	static SEARCH_ARRIVED_LABEL = 'Dnes dojely';
	static SEARCH_WILL_DEPARTURE_LABEL = 'Dnes pojedou';
	static SEARCH_TODAY_LABEL = 'Dnes';

	static SUBMIT_FORM_LABEL = 'Zobrazit';

	constructor(props) {
		super(props);
		this.state = { value: '' };
		this.state = { date: '' };
		this.state = { checkState: [false, false, false, false] };

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleDatePicker = this.handleDatePicker.bind(this);
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	handleCheckboxState = val => {
		const state = [false, false, false, false];
		state[val] = !this.state.checkState[val];
		this.setState({ checkState: state });
	};

	handleDatePicker(datePicker) {
		this.setState({ date: datePicker });
	}

	handleSubmit(event) {
		event.preventDefault();

		let time = SearchForm.DEPARTURE;
		const startOfDate = new Date();
		startOfDate.setHours(0, 0, 0, 0);
		const endOfDate = new Date();
		endOfDate.setHours(23, 59, 0, 0);
		const now = new Date();

		// Default Option is TODAY
		let since = getFormattedDate(startOfDate);
		let until = getFormattedDate(endOfDate);

		// Today Option
		if (this.state.checkState[0] === true) {
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(endOfDate);
		} else if (this.state.checkState[1] === true) {
			// Now
			since = getFormattedDate(now);
			until = getFormattedDate(now);
			time = SearchForm.ON_THE_WAY;
		} else if (this.state.checkState[2] === true) {
			// Arrived Today
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(now);
			time = SearchForm.ARRIVAL;
		} else if (this.state.checkState[3] === true) {
			// Will Departure Today
			since = getFormattedDate(now);
			until = getFormattedDate(endOfDate);
			time = SearchForm.DEPARTURE;
		} else if (this.state.date != null) {
			// Check date from datepicker
			const startOfSubmDate = new Date(this.state.date.getTime());
			startOfSubmDate.setHours(0, 0, 0, 0);
			const endOfSubmDate = new Date(this.state.date.getTime());
			endOfSubmDate.setHours(23, 59, 0, 0);
			since = getFormattedDate(startOfSubmDate);
			until = getFormattedDate(endOfSubmDate);
		}

		this.props.handleFilterTrainsets(since, until, time);
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="form-group">
					<Datepicker onFormChange={this.handleDatePicker} />
				</div>

				<div className="form-group">
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="todayCheckbox"
							value="todayOption"
							checked={this.state.checkState[0]}
							onChange={() => this.handleCheckboxState(0)}
						/>
						<label className="form-check-label" htmlFor="todayCheckbox">
							{SearchForm.SEARCH_TODAY_LABEL}
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="nowCheckbox"
							value="nowOption"
							checked={this.state.checkState[1]}
							onChange={() => this.handleCheckboxState(1)}
						/>
						<label className="form-check-label" htmlFor="nowCheckbox">
							{SearchForm.SEARCH_ON_THE_WAY_LABEL}
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="arrivedCheckbox"
							value="arrivedOption"
							checked={this.state.checkState[2]}
							onChange={() => this.handleCheckboxState(2)}
						/>
						<label className="form-check-label" htmlFor="arrivedCheckbox">
							{SearchForm.SEARCH_ARRIVED_LABEL}
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="willDepartureCheckbox"
							value="willDepartureOption"
							checked={this.state.checkState[3]}
							onChange={() => this.handleCheckboxState(3)}
						/>
						<label className="form-check-label" htmlFor="willDepartureCheckbox">
							{SearchForm.SEARCH_WILL_DEPARTURE_LABEL}
						</label>
					</div>

					<input type="submit" value={SearchForm.SUBMIT_FORM_LABEL} className="btn btn-primary" />
				</div>
			</form>
		);
	}
}

SearchForm.propTypes = {
	handleFilterTrainsets: PropTypes.func,
};

export default SearchForm;
