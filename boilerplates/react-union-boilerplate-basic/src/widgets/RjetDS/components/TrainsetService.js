import $ from 'jquery';
import toastr from 'toastr';
import getFormattedDate from './DateFormatter';

const URL_API_HOST = 'http://localhost:8080';
const TRAINSETS_PATH = 'soupravy';
const ROUTES_PATH = 'spoje';
const FIND = 'hledej';
const SINCE = 'od';
const UNTIL = 'do';
const TIME = 'cas';

const ROUTES_SCREEN = 'ROUTES_SCREEN';

const TRAINSETS_SUCCESSFULY_LOADED = 'Soupravy byly úspěšně načteny.';
const TRAINSETS_SUCCESSFULY_LOADED_NO_DATA = 'V daném časovém intervalu nebyly nalezeny žádné vlakové soupravy.';
const DELETED_SUCCESSFULY_LABEL = 'Souprava byla úspěšně smazána.';

const ARRIVAL = 'PRIJEZD';

const React = require('react');

class TrainsetService extends React.Component {
	static reloadTrainsetsFromServer(since, until, time) {
		if (typeof until === 'undefined' || typeof since === 'undefined' || typeof time === 'undefined') {
			const startOfDate = new Date();
			startOfDate.setHours(0, 0, 0, 0);
			const endOfDate = new Date();
			endOfDate.setHours(23, 59, 0, 0);
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(endOfDate);
			time = ARRIVAL;
		}
		const self = this;
		$.ajax({
			url: `${URL_API_HOST}/${TRAINSETS_PATH}/${FIND}?
			${SINCE}=${since}&${UNTIL}=${until}&${TIME}=${time}`,
		}).then(function(data) {
			self.setState({trainsets: data});
			if (typeof data === 'undefined' || data.length < 1) {
				toastr.success(TRAINSETS_SUCCESSFULY_LOADED_NO_DATA);
			} else {
				toastr.success(TRAINSETS_SUCCESSFULY_LOADED);
			}
		});
	}

	static loadTrainsetsFromServer() {
		const startOfDate = new Date();
		startOfDate.setHours(0, 0, 0, 0);
		const endOfDate = new Date();
		endOfDate.setHours(23, 59, 0, 0);
		const since = getFormattedDate(startOfDate);
		const until = getFormattedDate(endOfDate);
		const time = ARRIVAL;

		const self = this;
		$.ajax({
			url: `${URL_API_HOST}/${TRAINSETS_PATH}/
			${FIND}?${SINCE}=${since}&${UNTIL}=${until}&${TIME}=${time}`,
			/* TODO temp static URL - to delete after testing */
			// 'http://localhost:8080/soupravy/hledej?od=2017-01-01-1:30&do=2018-08-08-12:5&cas=prijezd',
		}).then(function(data) {
			self.setState({trainsets: data});
		});
	}

	static deleteTrainset() {
		const self = this;
		$.ajax({
			url: `${URL_API_HOST}/${TRAINSETS_PATH}/${self.props.trainset.id}`,
			type: 'DELETE',
			success() {
				self.setState({display: false});
				toastr.success(DELETED_SUCCESSFULY_LABEL);
			},
			error(xhr) {
				toastr.error(xhr.responseJSON.message);
			},
		});
	}

	static showRoutes(id) {
		const self = this;
		$.ajax({
			url: `${URL_API_HOST}/${TRAINSETS_PATH}/${id}/${ROUTES_PATH}`,
		}).then(function(data) {
            	self.setState({routes: data, mode: ROUTES_SCREEN });
		});
	}
}


export default TrainsetService;
