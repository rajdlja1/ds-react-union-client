import DatePicker from 'material-ui/DatePicker';
import PropTypes from 'prop-types';

const React = require('react');

class Datepicker extends React.Component {
	static CHOOSE_DATE_LABEL = 'Zvolte datum';

	constructor(props) {
		super(props);
		const minDate = new Date();
		const maxDate = new Date();
		minDate.setFullYear(minDate.getFullYear() - 1);
		minDate.setHours(0, 0, 0, 0);
		maxDate.setFullYear(maxDate.getFullYear() + 1);
		maxDate.setHours(0, 0, 0, 0);

		this.state = {
			minDate,
			maxDate,
			autoOk: true,
		};
	}

	handleDate = (event, date) => {
		this.setState({
			date,
		});
		this.props.onFormChange(date);
	};

	render() {
		return (
			<DatePicker
				onChange={this.handleDate}
				floatingLabelText={Datepicker.CHOOSE_DATE_LABEL}
				autoOk={this.state.autoOk}
				minDate={this.state.minDate}
				maxDate={this.state.maxDate}
			/>
		);
	}
}

Datepicker.propTypes = {
	onFormChange: PropTypes.func,
};

export default Datepicker;
