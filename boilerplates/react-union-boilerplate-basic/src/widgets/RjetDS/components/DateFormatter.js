/* Format suitable for REST api*/
function getFormattedDate(date) {
	const day = `0${date.getDate()}`.slice(-2);
	const month = `0${date.getMonth() + 1}`.slice(-2);
	const hours = `0${date.getHours()}`.slice(-2);
	const minutes = `0${date.getMinutes()}`.slice(-2);

	return `${date.getFullYear()}-${month}-${day}-${hours}:${minutes}`;
}

export default getFormattedDate;
