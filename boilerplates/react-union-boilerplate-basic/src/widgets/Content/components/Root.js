import React from 'react';

const Root = () => (
	<div className="jumbotron">
		<h1>RegioJet</h1>
		<p>Dispečingový systém pro plánování vlaků.</p>
	</div>
);

export default Root;
